package com.example.zookeeper_kevin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zookeeper_kevin.databinding.FragmentFinishBinding
import com.example.zookeeper_kevin.databinding.FragmentGettingStartedBinding

class FinishFragment : Fragment() {
    private var binding1: FragmentFinishBinding? = null
    private val binding2: FragmentFinishBinding get() = binding1!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFinishBinding.inflate(inflater, container, false).also {
        binding1= it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        finishinitlisteners()
    }
    private fun initListeners() = with(binding2) {
        returnbutt.setOnClickListener {
            val action = com.example.zookeeper_kevin.FinishFragmentDirections.actionFinishFragmentToGettingStarted()
            findNavController().navigate(action)
        }
    }
    private fun finishinitlisteners() = with(binding2) {
        nextbutt.setOnClickListener {
//            val action = com.example.zookeeper_kevin.FinishFragmentDirections.actionFinishFragmentToZooActivity()
          val direction = FinishFragmentDirections.actionFinishFragmentToZooActivity()
            findNavController().navigate(direction)

        }
    }
}
