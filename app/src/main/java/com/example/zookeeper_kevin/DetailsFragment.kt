package com.example.zookeeper_kevin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.zookeeper_kevin.databinding.FragmentDetailsBinding

class DetailsFragment : Fragment() {
    private var binding1: FragmentDetailsBinding? = null
    private val binding: FragmentDetailsBinding get() = binding1!!

    private val args by navArgs<DetailsFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailsBinding.inflate(inflater, container, false).also {
        binding1 = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        animalname.text = args.name
        animalfact.text = args.facts

    }
}