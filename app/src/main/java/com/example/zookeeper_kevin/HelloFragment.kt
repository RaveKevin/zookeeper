package com.example.zookeeper_kevin

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.zookeeper_kevin.databinding.FragmentHelloBinding


class HelloFragment:Fragment() {

    private var binding1: FragmentHelloBinding? = null
    private val binding: FragmentHelloBinding get() = binding1!!




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHelloBinding.inflate(inflater, container, false).also {
        binding1 = it}
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        nextbutton.setOnClickListener {
            val action = HelloFragmentDirections.actionHelloFragmentToGettingStarted2()
            findNavController().navigate(action)
        }
    }
}