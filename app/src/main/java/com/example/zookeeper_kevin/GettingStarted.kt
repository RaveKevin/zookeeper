package com.example.zookeeper_kevin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zookeeper_kevin.databinding.FragmentGettingStartedBinding

class GettingStarted : Fragment() {
    private var binding1: FragmentGettingStartedBinding? = null
    private val binding2: FragmentGettingStartedBinding get() = binding1!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingStartedBinding.inflate(inflater, container, false).also {
        binding1= it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        finishinitListeners()
    }
    private fun initListeners() = with(binding2) {
        returnbutt.setOnClickListener {
            val action = com.example.zookeeper_kevin.GettingStartedDirections.actionGettingStartedToHelloFragment2()
            findNavController().navigate(action)
        }
    }
    private fun finishinitListeners() = with(binding2) {
        nextbutt.setOnClickListener {
            val action = com.example.zookeeper_kevin.GettingStartedDirections.actionGettingStartedToFinishFragment()
            findNavController().navigate(action)
        }
    }
}