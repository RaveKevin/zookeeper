package com.example.zookeeper_kevin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zookeeper_kevin.databinding.FragmentHomeBinding

class HomeFragment:Fragment() {

    private var binding1: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = binding1!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        binding1 = it}
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dragoninitListeners()
        manticoreinitListeners()
        basiliskinitListeners()
        cerberusinitListeners()
    }

    private fun dragoninitListeners() = with(binding) {
         dragon.setOnClickListener {val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Dragon","The Dragon breathes fire and flies")
             findNavController().navigate(action)
            }
         }
    private fun basiliskinitListeners() = with(binding) {
        basilisk.setOnClickListener {val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Basilisk","n European bestiaries and legends, a basilisk is a legendary reptile reputed to be a serpent king, who can cause death with a single glance.")
            findNavController().navigate(action)
        }
    }
    private fun cerberusinitListeners() = with(binding) {
        cerberus.setOnClickListener {val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Cerberus","Hades pet dog with 3 heads")
            findNavController().navigate(action)
        }
    }
    private fun manticoreinitListeners() = with(binding) {
        manticore.setOnClickListener {val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Manticore","a legendary animal having the head of a man (often with horns), the body of a lion, and the tail of a dragon or scorpion.")
            findNavController().navigate(action)
        }
    }
}
